# Publishing

* [ ] Update `CHANGELOG.markdown`
* [ ] Update `README.markdown` "Usage" section (constrain to 70 characters wide)
* [ ] Update `package.json` version number
* [ ] Update `package-lock.json` version number
* [ ] `npm publish --dry-run` and review list of files
* [ ] `git commit -m v#.#.#`
* [ ] `git tag v#.#.#`
* [ ] `git push --tags origin main`
* [ ] `npm publish`
