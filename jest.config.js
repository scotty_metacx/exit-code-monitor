module.exports = {
  verbose: false,
  testMatch: ['<rootDir>/src/**/*.unit.test.js'],
  reporters: [
    'default',

    // Additional for GitLab
    'jest-junit'
  ],

  collectCoverageFrom: ['src/**/*.js'],
  coveragePathIgnorePatterns: [
    '.test.js$',
    '/__fixtures__/'
  ],
  coverageReporters: [
    // Additional for GitLab
    'cobertura',

    // defaults:
    'clover',
    'json',
    'lcov',
    'text'
  ]
}
