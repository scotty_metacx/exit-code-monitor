/* eslint-env jest */
jest.mock('fs')

const { createWriteStream } = require('fs')

const { PrefixStream, openFileOutputs } = require('./file-output')
const { Writable, finished } = require('stream')

const TEST_PREFIX = 'TESTING_123: '

beforeEach(() => {
  createWriteStream.mockReset()
  createWriteStream.mockImplementation(filename => {
    const stream = new Writable({
      _write: (chunk, encoding, callback) => {
        this.data = (this.data ?? '') + Buffer.from(chunk, encoding).toString('utf8')
        callback(null)
      }
    })

    jest.spyOn(stream, 'end')

    return stream
  })
})

describe('PrefixStream', () => {
  describe('constructor', () => {
    it('should store the provided options', () => {
      const stream = new PrefixStream({ prefix: TEST_PREFIX })
      expect(stream.prefix).toBe(TEST_PREFIX)
      expect(stream.buffer).toBe(TEST_PREFIX)
    })
  })

  describe('_transform', () => {
    it('should convert non-utf8 strings into utf8 strings', () => {
      const testString = 'Hello world\n'
      const chunk = Buffer.from(testString).toString('base64')
      const stream = new PrefixStream({ prefix: TEST_PREFIX })
      const callback = jest.fn()
      stream._transform(chunk, 'base64', callback)
      expect(callback).toHaveBeenCalledTimes(1)
      expect(callback).toHaveBeenCalledWith(
        null,
        TEST_PREFIX + testString
      )
    })

    it('should convert buffers into utf8 strings', () => {
      const testString = 'Hello world\n'
      const chunk = Buffer.from(testString)
      const stream = new PrefixStream({ prefix: TEST_PREFIX })
      const callback = jest.fn()
      stream._transform(chunk, 'buffer', callback)
      expect(callback).toHaveBeenCalledTimes(1)
      expect(callback).toHaveBeenCalledWith(
        null,
        TEST_PREFIX + testString
      )
    })

    it('should accept utf8 strings', () => {
      const testString = 'Hello world\n'
      const stream = new PrefixStream({ prefix: TEST_PREFIX })
      const callback = jest.fn()
      stream._transform(testString, 'utf8', callback)
      expect(callback).toHaveBeenCalledTimes(1)
      expect(callback).toHaveBeenCalledWith(
        null,
        TEST_PREFIX + testString
      )
    })

    it('should keep the data in the buffer if a newline has not yet arrived', () => {
      const testString = 'Hello world'
      const stream = new PrefixStream({ prefix: TEST_PREFIX })
      const callback = jest.fn()
      stream._transform(testString, 'utf8', callback)
      expect(callback).toHaveBeenCalledTimes(1)
      expect(callback).toHaveBeenCalledWith(null, undefined)
      expect(stream.buffer).toBe(TEST_PREFIX + testString)
    })

    it('should only emit complete lines', () => {
      const wholeLine = 'Hello world\n'
      const partialString = 'foobar'
      const stream = new PrefixStream({ prefix: TEST_PREFIX })
      const callback = jest.fn()
      stream._transform(wholeLine + partialString, 'utf8', callback)
      expect(callback).toHaveBeenCalledTimes(1)
      expect(callback).toHaveBeenCalledWith(
        null,
        TEST_PREFIX + wholeLine
      )
      expect(stream.buffer).toBe(TEST_PREFIX + partialString)
    })
  })

  describe('flush', () => {
    it('should flush the buffer if there is any non-prefix content', () => {
      const stream = new PrefixStream({ prefix: TEST_PREFIX })
      stream.buffer = stream.prefix + 'partial'
      const callback = jest.fn()
      stream._flush(callback)
      expect(callback).toHaveBeenCalledTimes(1)
      expect(callback).toHaveBeenCalledWith(
        null,
        TEST_PREFIX + 'partial\n'
      )
    })

    it('should not emit a line if the buffer only contains the prefix', () => {
      const stream = new PrefixStream({ prefix: TEST_PREFIX })
      const callback = jest.fn()
      stream._flush(callback)
      expect(callback).toHaveBeenCalledTimes(1)
      expect(callback).toHaveBeenCalledWith(null)
    })
  })

  it('should only emit whole lines prefixed with the provided string', done => {
    const stream = new PrefixStream({ prefix: TEST_PREFIX })

    let data = ''
    stream.on('data', chunk => {
      data += chunk
    })

    stream.write('partial')
    stream.write('_line\nwith_another_partial')
    stream.write('_line\nline3\nline4\nline5\n')
    stream.write('\nline7')
    stream.end()

    finished(stream, err => {
      expect(err).toBe(undefined)
      expect(data).toBe(
        `${TEST_PREFIX}partial_line\n` +
        `${TEST_PREFIX}with_another_partial_line\n` +
        `${TEST_PREFIX}line3\n` +
        `${TEST_PREFIX}line4\n` +
        `${TEST_PREFIX}line5\n` +
        `${TEST_PREFIX}\n` +
        `${TEST_PREFIX}line7\n`
      )
      done()
    })
  })
})

describe('openFileOutputs', () => {
  let commandSpecs

  beforeEach(() => {
    commandSpecs = [
      { label: 'nopipes1' },
      { label: 'nopipes2' },
      { label: 'emptystr', stdout: '', stderr: '' },
      { label: 'stdout1', stdout: 'file1' },
      { label: 'stdout2', stdout: 'file2' },
      { label: 'stdout3', stdout: 'file2' },
      { label: 'stderr1', stderr: 'file3' },
      { label: 'stderr2', stderr: 'foo/../file4' },
      { label: 'stderr3', stderr: 'bar/../file4' },
      { label: 'both1', stdout: 'file5', stderr: 'file5' },
      { label: 'both2', stdout: 'file6', stderr: 'file7' }
    ]
  })

  it('should safely ignore pipes without a destination filename', async () => {
    // Only keep the first three
    commandSpecs.splice(3, commandSpecs.length - 3)

    const {
      fileDescriptors,
      allPrefixStreams,
      allPipesFinishedPromise
    } = openFileOutputs(commandSpecs)

    expect(createWriteStream).not.toHaveBeenCalled()

    expect(commandSpecs[0].stdout).toBe(undefined)
    expect(commandSpecs[0].stderr).toBe(undefined)
    expect(commandSpecs[1].stdout).toBe(undefined)
    expect(commandSpecs[1].stderr).toBe(undefined)
    expect(commandSpecs[2].stdout).toBe('')
    expect(commandSpecs[2].stderr).toBe('')

    expect(fileDescriptors.size).toBe(0)
    expect(allPrefixStreams.length).toBe(0)
    expect(allPipesFinishedPromise).toBeInstanceOf(Promise)
    await allPipesFinishedPromise // should immediately complete
  })

  it('should open an append stream for each unique file', () => {
    const {
      fileDescriptors,
      allPrefixStreams
    } = openFileOutputs(commandSpecs)

    const expectedFiles = ['file1', 'file2', 'file3', 'file4', 'file5', 'file6', 'file7']

    expect(createWriteStream).toHaveBeenCalledTimes(7)

    expectedFiles.forEach(file => {
      const fileWithPath = process.cwd() + '/' + file

      expect(createWriteStream).toHaveBeenCalledWith(
        fileWithPath,
        expect.objectContaining({ flags: 'a' })
      )

      expect(fileDescriptors.get(fileWithPath)).toBeInstanceOf(Writable)
    })

    commandSpecs.forEach(spec => {
      if (spec.stdout) {
        expect(spec.stdout).toBeInstanceOf(PrefixStream)
        expect(allPrefixStreams).toContain(spec.stdout)
      }

      if (spec.stderr) {
        expect(spec.stderr).toBeInstanceOf(PrefixStream)
        expect(allPrefixStreams).toContain(spec.stderr)
      }
    })
  })

  it('should close all file descriptors when all prefix streams are finished', done => {
    const {
      fileDescriptors,
      allPrefixStreams
    } = openFileOutputs(commandSpecs)

    allPrefixStreams.forEach(prefixStream => prefixStream.end())

    // timeouts have lower priority than promises, so this should allow all
    // promises to complete
    setTimeout(() => {
      fileDescriptors.forEach(fd =>
        expect(fd.end).toHaveBeenCalledTimes(1)
      )

      done()
    }, 1)
  })

  it('should export a promsise for when all file descriptors are closed', async () => {
    const {
      allPrefixStreams,
      allPipesFinishedPromise
    } = openFileOutputs(commandSpecs)

    allPrefixStreams.forEach(prefixStream => prefixStream.end())

    await allPipesFinishedPromise
    // If we get here without timing out, then it worked
  })
})
