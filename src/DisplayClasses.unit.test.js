/* eslint-env jest */
const { Interactive } = require('./Interactive')
const { NonInteractive } = require('./NonInteractive')

describe('Display class', () => {
  describe.each([
    ['Interactive', Interactive],
    ['NonInteractive', NonInteractive]
  ])('%s', (name, constructor) => {
    it('should be a constructor', () => {
      expect(constructor.prototype).toBeTruthy()
    })

    it('should provide a setup method', () => {
      expect(typeof constructor.prototype.setup).toBe('function')
    })

    it('should provide a shutdown method', () => {
      expect(typeof constructor.prototype.shutdown).toBe('function')
    })
  })
})
