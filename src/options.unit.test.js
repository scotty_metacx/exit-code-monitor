/* eslint-env jest */
process.env.LC_ALL = 'en'

const { pick } = require('lodash')

beforeEach(() => {
  jest.resetModules()

  Object.keys(process.env).forEach(envVar => {
    if (envVar.indexOf('EXIT_CODE_MONITOR_') === 0) {
      delete process.env[envVar]
    }
  })
})

describe.each([
  'APP_OPTIONS',
  'COMMAND_OPTIONS',
  'CARRY_FORWARD_OPTIONS'
])('%s', constant => {
  it('should be an array of strings (option names)', () => {
    const test = require('./options')[constant]
    expect(Array.isArray(test)).toBe(true)
    for (const value of test) {
      expect(typeof value).toBe('string')
    }
  })
})

describe('CARRY_FORWARD_OPTIONS', () => {
  it('should not include label', () => {
    const { CARRY_FORWARD_OPTIONS } = require('./options')
    expect(CARRY_FORWARD_OPTIONS.includes('label')).toBe(false)
  })
})

describe('DEFAULTS', () => {
  let originalProcessPlatform

  beforeEach(() => {
    originalProcessPlatform = process.platform
  })

  afterEach(() => {
    Object.defineProperty(process, 'platform', {
      value: originalProcessPlatform
    })
  })

  it('should be an object of app and command options with values', () => {
    const { CARRY_FORWARD_OPTIONS, DEFAULTS } = require('./options')

    expect(typeof DEFAULTS).toBe('object')
    Object.keys(DEFAULTS).forEach(key =>
      expect(CARRY_FORWARD_OPTIONS.includes(key)).toBe(true)
    )
  })

  describe('on non-windows', () => {
    it('should default to the NUL devices for logging', () => {
      Object.defineProperty(process, 'platform', { value: 'other' })
      const { DEFAULTS } = require('./options')
      expect(DEFAULTS.log).toBe('/dev/null')
    })
  })

  describe('on windows', () => {
    it('should default to the NUL devices for logging', () => {
      Object.defineProperty(process, 'platform', { value: 'win32' })
      const { DEFAULTS } = require('./options')
      expect(DEFAULTS.log).toMatch(/NUL/)
    })
  })
})

describe('MODE_SHORTHAND', () => {
  it('should map short mode names to full names', () => {
    const { MODE_SHORTHAND } = require('./options')

    expect(typeof MODE_SHORTHAND).toBe('object')
    Object.values(MODE_SHORTHAND).forEach(value =>
      expect(typeof value).toBe('string')
    )
  })
})

describe('checkArgs', () => {
  let argv

  beforeEach(() => {
    const { DEFAULTS } = require('./options')
    argv = { ...DEFAULTS }
  })

  describe.each([
    { option: 'code', nan: true, min: 0, max: 255 },
    { option: 'consecutive', nan: false, min: 1, max: null },
    { option: 'timeout', nan: false, min: 1, max: null },
    { option: 'timeoutcode', nan: true, min: 0, max: 255 },
    { option: 'delay', nan: false, min: 1, max: null },
    { option: 'report', nan: false, min: 1, max: null },
    { option: 'rate', nan: false, min: 100, max: null },
    { option: 'limit', nan: false, min: 100, max: null }
  ])('$option', ({ option, nan, min, max }) => {
    if (nan) {
      it('may be NaN', () => {
        const { checkArgs } = require('./options')

        argv[option] = NaN
        expect(checkArgs(argv)).toBe(true)
      })
    } else {
      it('may not be NaN', () => {
        const { checkArgs } = require('./options')

        argv[option] = NaN
        expect(checkArgs(argv)).toMatch(option)
      })
    }

    if (min !== null) {
      it('may not be less than ' + min, () => {
        const { checkArgs } = require('./options')
        argv[option] = min - 0.001
        expect(checkArgs(argv)).toMatch(option)
      })
    }

    if (max !== null) {
      it('may not be more than ' + max, () => {
        const { checkArgs } = require('./options')
        argv[option] = max + 0.001
        expect(checkArgs(argv)).toMatch(option)
      })
    }
  })

  describe('pwd', () => {
    let fs
    let isDirectory

    beforeEach(() => {
      isDirectory = jest.fn()

      fs = require('fs')
      jest.spyOn(fs, 'statSync')
      fs.statSync.mockReturnValue({ isDirectory })
    })

    afterEach(() => {
      fs.statSync.mockRestore()
    })

    it('should be a valid directory', () => {
      isDirectory.mockReset()
      isDirectory.mockReturnValueOnce(false)
      isDirectory.mockReturnValueOnce(true)

      const { DEFAULTS, checkArgs } = require('./options')

      expect(checkArgs(DEFAULTS)).toMatch('pwd')
      expect(checkArgs(DEFAULTS)).toBe(true)
    })

    it('should call statSync with throwIfNoEntry:false', () => {
      isDirectory.mockReturnValue(true)

      const { DEFAULTS, checkArgs } = require('./options')

      checkArgs(DEFAULTS)

      expect(fs.statSync).toHaveBeenCalledWith(
        DEFAULTS.pwd,
        {
          throwIfNoEntry: false
        }
      )
    })
  })

  it('should fail if rate is less than limit', () => {
    const { DEFAULTS, checkArgs } = require('./options')
    const argv = {
      ...DEFAULTS,
      rate: 100,
      limit: 101
    }

    const result = checkArgs(argv)
    expect(result).toMatch('rate')
    expect(result).toMatch('limit')
  })
})

describe('cleanupPath', () => {
  it('should filter out empty paths', () => {
    const { cleanupPath } = require('./options')
    expect(cleanupPath(':::a::b::c:::')).toBe('a:b:c')
  })

  it('should remove duplicate paths', () => {
    const { cleanupPath } = require('./options')
    expect(cleanupPath('a:a:b:b:a:b:c:c:c:a:b:c')).toBe('a:b:c')
  })
})

describe('parser', () => {
  it('should support environment variables', () => {
    const { DEFAULTS, parser } = require('./options')
    expect(DEFAULTS.verbose).not.toBe(true)
    process.env.EXIT_CODE_MONITOR_VERBOSE = 'true'
    const parsed = parser.parse(['command1'])
    expect(parsed.verbose).toBe(true)
  })

  it('should reject unknown options', () => {
    const { parser } = require('./options')
    expect(parser.getStrict()).toBe(true)
  })

  it('should override previous options instead of collecting them into an array', () => {
    const { parser } = require('./options')
    const config = parser.getInternalMethods().getParserConfiguration()
    expect(config['duplicate-arguments-array']).toBe(false)
  })

  it('should stop processing at the first non-option so we can determine options as of each command', () => {
    const { parser } = require('./options')
    const config = parser.getInternalMethods().getParserConfiguration()
    expect(config['halt-at-non-option']).toBe(true)
  })
})

describe('parseArgv', () => {
  let originalArgv
  let appDefaults
  let commandDefaults
  let originalProcessExit

  beforeEach(() => {
    originalArgv = process.argv
    process.argv = process.argv.slice(0, 2)

    const { DEFAULTS, APP_OPTIONS, COMMAND_OPTIONS } = require('./options')
    appDefaults = pick(DEFAULTS, APP_OPTIONS)
    commandDefaults = pick(DEFAULTS, COMMAND_OPTIONS)
    commandDefaults.path = process.env.PATH

    originalProcessExit = process.exit
    process.exit = jest.fn()

    jest.spyOn(console, 'error')
    console.error.mockImplementation(() => {})
  })

  afterEach(() => {
    console.error.mockRestore()
    process.exit = originalProcessExit
    process.argv = originalArgv
  })

  it('should carry forward options to subsequent commands until they are overridden', () => {
    const { parseArgv } = require('./options')

    process.argv.push(
      '--mode', 'fail',
      'command1',
      'command2',
      '--mode', 'info',
      'command3',
      'command4'
    )

    expect(parseArgv()).toEqual({
      commandSpecs: [
        { ...commandDefaults, label: 'command1', command: 'command1', mode: 'fail' },
        { ...commandDefaults, label: 'command2', command: 'command2', mode: 'fail' },
        { ...commandDefaults, label: 'command3', command: 'command3', mode: 'info' },
        { ...commandDefaults, label: 'command4', command: 'command4', mode: 'info' }
      ],
      ...appDefaults
    })
  })

  it('should report usage if no commands provided', () => {
    const { parseArgv } = require('./options')

    parseArgv()

    expect(process.exit).toHaveBeenCalled()
    expect(console.error).toHaveBeenCalledWith(
      expect.stringMatching('At least one command must be provided')
    )
  })

  it('should report usage if any command is an empty string', () => {
    const { parseArgv } = require('./options')

    process.argv.push('')

    parseArgv()

    expect(process.exit).toHaveBeenCalled()
    expect(console.error).toHaveBeenCalledWith(
      expect.stringMatching('commands must not be empty')
    )
  })

  it('should cleanup the path', () => {
    const { parseArgv } = require('./options')

    process.argv.push('--path', '::foo::bar::foo', 'command1')

    expect(parseArgv()).toEqual({
      commandSpecs: [
        {
          ...commandDefaults,
          label: 'command1',
          command: 'command1',
          path: 'foo:bar:' + process.env.PATH
        }
      ],
      ...appDefaults
    })
  })

  it('should convert shorthand modes to the full name', () => {
    const { parseArgv } = require('./options')

    process.argv.push('--mode', 'f', 'command1')

    expect(parseArgv()).toEqual({
      commandSpecs: [
        { ...commandDefaults, label: 'command1', command: 'command1', mode: 'fail' }
      ],
      ...appDefaults
    })
  })
})
