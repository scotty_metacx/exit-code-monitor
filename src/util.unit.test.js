/* eslint-env jest */
const { createPromise, formatRuntime } = require('./util')

describe('createPromise', () => {
  it('should return a promise, its resolve, and its reject methods', () => {
    const returned = createPromise()
    expect(returned).toEqual({
      promise: expect.any(Promise),
      resolve: expect.any(Function),
      reject: expect.any(Function)
    })
  })
})

describe('formatRuntime', () => {
  it('should round to the nearest decisecond', () => {
    expect(formatRuntime(0, 349)).toBe('0.3s')
    expect(formatRuntime(0, 350)).toBe('0.4s')
  })

  it('should append .0 to whole numbers', () => {
    expect(formatRuntime(0, 1000)).toBe('1.0s')
  })
})
