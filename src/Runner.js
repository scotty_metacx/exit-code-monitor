const { spawn } = require('child_process')
const EventEmitter = require('events')
const { pick, omit } = require('lodash')
const { createPromise } = require('./util')

class Runner extends EventEmitter {
  // spec === commandSpec
  constructor (spec, logger) {
    super()
    this.killSignal = Runner.KILL_SIGNAL
    this.spec = spec
    this.logger = logger
    this.running = false
    this.last = null
    this.consecutive = 0
    this.current = null
    this.delayHandle = null
    this.firstMet = null
  }

  logProperties () {
    return {
      ...pick(this, [
        'killSignal',
        'running',
        'last',
        'consecutive',
        'firstMet'
      ]),
      current: this.current && {
        ...omit(this.current, [
          'promise',
          'child',
          'kill'
        ]),
        child: pick(this.current.child, [
          'pid'
        ])
      },
      spec: omit(this.spec, [
        'stdout',
        'stderr'
      ])
    }
  }

  determineState ({ err, timedOut, code }) {
    if (timedOut) {
      // If the user wants to treat timeout as a specific exit code, then treat
      // timeout as explicitly exited with that code
      if (!Number.isNaN(this.spec.timeoutcode)) {
        return {
          state: Runner.EXIT_STATE_EXITED,
          code: this.spec.timeoutcode
        }
      }

      return {
        state: Runner.EXIT_STATE_TIMEOUT,
        code: NaN
      }
    }

    if (err || typeof code !== 'number') {
      return {
        state: Runner.EXIT_STATE_OTHER,
        code: NaN
      }
    }

    return {
      state: Runner.EXIT_STATE_EXITED,
      code: parseInt(code, 10)
    }
  }

  determineCodeClass (code) {
    return code === 0
      ? Runner.EXIT_CODE_CLASS_PASS
      : Runner.EXIT_CODE_CLASS_FAIL
  }

  interpretResult ({ state, code, codeClass }) {
    // Interpret state to determine if it meets the expectations of the
    // provided spec for the command
    switch (this.spec.mode) {
      case 'pass':
        return state === Runner.EXIT_STATE_EXITED && codeClass === Runner.EXIT_CODE_CLASS_PASS
          ? Runner.EXIT_RESULT_MET
          : Runner.EXIT_RESULT_UNMET
      case 'fail':
        return state === Runner.EXIT_STATE_EXITED && codeClass === Runner.EXIT_CODE_CLASS_FAIL
          ? Runner.EXIT_RESULT_MET
          : Runner.EXIT_RESULT_UNMET
      case 'code':
        return state === Runner.EXIT_STATE_EXITED && code === this.spec.code
          ? Runner.EXIT_RESULT_MET
          : Runner.EXIT_RESULT_UNMET
      case 'info':
        return Runner.EXIT_RESULT_IGNORE
    }

    return Runner.EXIT_RESULT_UNMET
  }

  shouldKeepGoing () {
    const lastResult = this.last?.result
    const resultNotMet = lastResult !== Runner.EXIT_RESULT_MET && lastResult !== Runner.EXIT_RESULT_IGNORE
    const consecutiveNotMet = this.consecutive < this.spec.consecutive
    return resultNotMet || consecutiveNotMet
  }

  // Returns {
  //   promise
  //   child
  //   kill
  //   started: Date.now()
  // }
  //
  // Resolves with an object {
  //   state: EXIT_STATE_EXITED | EXIT_STATE_TIMEOUT | EXIT_STATE_OTHER
  //   code: Number | NaN
  //   codeClass: EXIT_CODE_CLASS_PASS | EXIT_CODE_CLASS_FAIL
  //   result: EXIT_RESULT_MET | EXIT_RESULT_UNMET | EXIT_RESULT_IGNORE
  //   started: Date.now()
  //   ended: Date.now()
  // }
  //
  // Note on killing subprocess group:
  // https://azimi.me/2014/12/31/kill-child_process-node-js.html
  runCommand () {
    // Extract the config we need
    const { command, pwd, path, stdout, stderr, timeout } = this.spec

    // Create a promise for this execution
    const { promise, resolve } = createPromise()

    // Record start time
    const started = Date.now()

    // If the command is a single word, no need for a subshell. Otherwise
    // interpret the command.
    const shell = command.includes(' ')

    // Actually spawn the command
    const child = spawn(command, {
      detached: true,
      shell,
      cwd: pwd,
      env: { ...process.env, PATH: path },
      stdio: [
        'ignore', // stdin
        stdout ? 'pipe' : 'ignore',
        stderr ? 'pipe' : 'ignore'
      ]
    })

    // Forward all data from child processes to the defined file descriptors
    if (stdout) {
      child.stdout.pipe(stdout, { end: false })
    }
    if (stderr) {
      child.stderr.pipe(stderr, { end: false })
    }

    // Callback to forcefully end the child process
    const kill = () => {
      // Prevent timeout from firing
      clearTimeout(timeoutHandle)

      try {
        // Kill entire child pid group (ensures subshell commands are killed)
        process.kill(-child.pid, this.killSignal)
      } catch (e) {
        // process.kill() throws if child process no longer exists, which is
        // what we wanted, so ignore it
      }
    }

    // Would use `timeout` option of `spawn` but it only lets us know the
    // process was killed, not that it was killed because of timeout. Therefore
    // implement our own timeout so we can keep track of it.
    let timedOut = false
    const timeoutHandle = setTimeout(() => {
      timedOut = true
      kill()
    }, timeout * 1000)

    // err: kill signal, error spawning, or error killing
    const handleExit = (err, literalCode) => {
      const now = Date.now()

      clearTimeout(timeoutHandle)

      // Determine exit state, taking into account timeout may override code
      const { state, code } =
        this.determineState({ err, timedOut, code: literalCode })

      // In shell parlance, success or truth has the exit code zero
      const codeClass = this.determineCodeClass(code)

      // Interpret the above state to determine if it meets the expectations of
      // the provided spec for the command
      const result = this.interpretResult({ state, code, codeClass })

      resolve({
        state,
        code,
        codeClass,
        result,
        started,
        ended: now
      })
    }

    // Process could not be spawned or was killed
    child.on('error', err => {
      handleExit(err)
    })

    // code null or number
    // signal null or string
    child.on('exit', (code, signal) => {
      handleExit(signal, code)
    })

    this.current = { promise, child, kill, started }
    promise.then(resolved => this.onCommandComplete(resolved))

    // Let others know data has changed
    this.logger.debug('Running command', this.logProperties())
    this.emit('updated', this)
  }

  onCommandComplete (resolved) {
    // Clear tracking of in-progress command
    this.current = null

    // If we were stopped, ignore completion of command
    if (!this.running) {
      return
    }

    if (
      this?.last?.state !== resolved.state ||
      this?.last?.result !== resolved.result
    ) {
      this.consecutive = 1
    } else {
      this.consecutive++
    }

    // Record result of last run
    this.last = resolved
    if (resolved.result === Runner.EXIT_RESULT_UNMET) {
      this.firstMet = null
    } else if (resolved.result === Runner.EXIT_RESULT_MET) {
      this.firstMet = this.firstMet || resolved.ended
    }

    // Let others know data has changed
    this.logger.debug('Completed command', this.logProperties())
    this.emit('updated')

    // If we should auto-stop
    if (this.spec.latch && !this.shouldKeepGoing()) {
      this.stop()
    } else {
      // Schedule the next run
      this.delayHandle = setTimeout(() => {
        this.clearDelay()
        this.runCommand()
      }, this.spec.delay * 1000)
    }
  }

  clearDelay () {
    clearTimeout(this.delayHandle)
    this.delayHandle = null
  }

  start () {
    if (this.running) {
      return
    }

    this.logger.verbose('Starting runner', this.logProperties())

    this.running = true
    this.runCommand()
  }

  stop () {
    this.logger.verbose('Stopping runner', this.logProperties())

    this.running = false
    this.current?.kill()
    this.current = null
    this.clearDelay()
    this.emit('updated')
  }

  toggle () {
    if (this.running) {
      this.stop()
    } else {
      this.start()
    }
  }
}

Runner.KILL_SIGNAL = 'SIGKILL'

;[
  'EXIT_STATE_EXITED',
  'EXIT_STATE_TIMEOUT',
  'EXIT_STATE_OTHER',
  'EXIT_CODE_CLASS_PASS',
  'EXIT_CODE_CLASS_FAIL',
  'EXIT_RESULT_MET',
  'EXIT_RESULT_UNMET',
  'EXIT_RESULT_IGNORE'
].forEach(constant => {
  Runner[constant] = constant
})

module.exports = {
  Runner
}
