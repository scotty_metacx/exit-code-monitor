const { createWriteStream } = require('fs')
const { resolve } = require('path')
const { Transform, finished } = require('stream')

class PrefixStream extends Transform {
  constructor (options) {
    super(options)
    this.prefix = options.prefix
    this.buffer = this.prefix
  }

  _transform (chunk, encoding, callback) {
    // Force utf8 encoding
    if (typeof chunk !== 'string' || encoding !== 'utf8') {
      if (!Buffer.isBuffer(chunk)) {
        chunk = Buffer.from(chunk, encoding)
      }

      chunk = chunk.toString('utf8')
    }

    // .split().join() is significantly faster than .replace() despite the
    // memory overhead for array creation
    const lines = (this.buffer + chunk).split('\n')

    // Keep incomplete lines in the buffer.
    const lastLine = lines.pop()
    this.buffer = (lines.length ? this.prefix : '') + lastLine

    // Take all completed lines and emit them.
    let data
    if (lines.length) {
      // First line should already be prefixed because of initialized buffer,
      // and since we only emit whole lines, the last line needs an additional
      // newline at the end
      data = lines.join('\n' + this.prefix) + '\n'
    }

    callback(null, data)
  }

  _flush (callback) {
    if (this.buffer && this.buffer !== this.prefix) {
      callback(null, this.buffer + '\n')
    } else {
      callback(null)
    }
  }
}

const openFileOutputs = commandSpecs => {
  const fileDescriptors = new Map()
  const allPrefixStreams = []

  commandSpecs.forEach(commandSpec => {
    ['stdout', 'stderr'].forEach(pipeName => {
      let file = commandSpec[pipeName]
      if (!file) {
        return
      }

      file = resolve(file)

      if (!fileDescriptors.has(file)) {
        fileDescriptors.set(file, createWriteStream(file, { flags: 'a' }))
      }

      commandSpec[pipeName] = new PrefixStream({ prefix: commandSpec.label + ': ' })
      commandSpec[pipeName].pipe(fileDescriptors.get(file), { end: false })
      allPrefixStreams.push(commandSpec[pipeName])
    })
  })

  Promise.all(
    allPrefixStreams.map(
      prefixStream => new Promise(resolve => finished(prefixStream, resolve))
    )
  ).then(() => {
    for (const fd of fileDescriptors.values()) {
      fd.end()
    }
  })

  const allPipesFinishedPromise = Promise.all(
    Array.from(fileDescriptors.values()).map(
      fd => new Promise(resolve => finished(fd, resolve))
    )
  )

  return { fileDescriptors, allPrefixStreams, allPipesFinishedPromise }
}

module.exports = {
  PrefixStream,
  openFileOutputs
}
