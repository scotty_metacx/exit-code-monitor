const winston = require('winston')
const { Interactive } = require('./Interactive')
const { NonInteractive, printExitSummary } = require('./NonInteractive')
const { Runner } = require('./Runner')
const { parseArgv } = require('./options')
const { openFileOutputs } = require('./file-output')

class App {
  constructor () {
    this.started = Date.now()
    this.options = null
    this.logger = null
    this.fileDescriptors = null
    this.allPrefixStreams = null
    this.allPipesFinishedPromise = null
    this.runners = null
    this.display = null
    this.killed = false
    this.allRunnersInfo = false

    this.onRunnerUpdated = this.onRunnerUpdated.bind(this)
    this.exit = this.exit.bind(this)
  }

  // Should keep going if any runner should keep going
  shouldKeepGoing () {
    return Boolean(this.runners.find(runner => runner.shouldKeepGoing()))
  }

  async exit () {
    if (this.killed) {
      return
    }
    this.killed = true

    if (!this.allRunnersInfo && this.shouldKeepGoing()) {
      process.exitCode = 1
    }

    const allCurrentCommandsComplete = Promise.all(
      this.runners.map(runner =>
        runner?.current?.promise
      ).filter(promise => Boolean(promise))
    )

    this.runners.forEach(runner => runner.stop())

    this.display?.shutdown()
    if (!this.options.quiet) {
      printExitSummary(this)
    }

    this.logger.log({
      level: process.exitCode ? 'warn' : 'info',
      message: 'Exiting',
      started: this.started,
      exited: Date.now(),
      allRunnersInfo: this.allRunnersInfo,
      exitCode: process.exitCode || 0,
      runners: this.runners.map(runner => runner.logProperties())
    })

    await allCurrentCommandsComplete
    this.allPrefixStreams.forEach(prefixStream => prefixStream.end())
  }

  onRunnerUpdated () {
    if (this.killed || this.allRunnersInfo || this.shouldKeepGoing()) {
      return
    }

    this.logger.verbose('All commands met expectations')

    this.exit()
  }

  setupOptions () {
    this.options = parseArgv()
  }

  setupLogger () {
    this.logger = winston.createLogger({
      levels: winston.config.npm.levels,
      level: this.options.level,
      format: winston.format.json(),
      transports: [
        new winston.transports.File({ filename: this.options.log })
      ]
    })

    this.logger.info('Starting', {
      started: this.started,
      argv: process.argv,
      options: this.options
    })
  }

  setupFileOutputs () {
    // Sets fileDescriptors, allPrefixStreams, allPipesFinishedPromise
    Object.assign(this, openFileOutputs(this.options.commandSpecs))
  }

  setupRunners () {
    this.runners = []
    this.options.commandSpecs.forEach(spec => {
      const runner = new Runner(spec, this.logger)
      runner.on('updated', this.onRunnerUpdated)
      this.runners.push(runner)
    })
  }

  setAllRunnersInfo () {
    // For efficiency, evaluate this once up-front. If we can't find any
    // non-info runner, then all must be info.
    this.allRunnersInfo = !this.runners.find(
      runner => runner.spec.mode !== 'info'
    )
  }

  setupDisplay () {
    if (!this.options.quiet) {
      if (this.options.tty) {
        this.display = new Interactive(this)
      } else {
        this.display = new NonInteractive(this)
      }
    }

    this.display?.setup()
  }

  startRunners () {
    this.runners.forEach(runner => runner.start())
  }

  main () {
    this.setupOptions()
    this.setupLogger()
    this.setupFileOutputs()
    this.setupRunners()
    this.setAllRunnersInfo()
    this.setupDisplay()
    this.startRunners()
  }
}

module.exports = { App }
