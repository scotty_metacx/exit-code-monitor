/* eslint-env jest */
jest.mock('./App')
const { App } = require('./App')

describe('the index file', () => {
  it('should instantiate and execute the app', () => {
    expect(App).not.toHaveBeenCalled()
    expect(App.prototype.main).not.toHaveBeenCalled()

    require('./index')

    expect(App).toHaveBeenCalledTimes(1)
    expect(App.prototype.main).toHaveBeenCalledTimes(1)
  })
})
