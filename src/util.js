const createPromise = () => {
  let res, rej
  const promise = new Promise((resolve, reject) => {
    res = resolve
    rej = reject
  })
  return { promise, resolve: res, reject: rej }
}

const formatRuntime = (started, ended) => {
  let runtime = String(Math.round((ended - started) / 100) / 10)
  runtime += runtime.includes('.') ? '' : '.0'
  return runtime + 's'
}

module.exports = { createPromise, formatRuntime }
