/* eslint-env jest */
jest.mock('winston')
jest.mock('./Interactive')
jest.mock('./NonInteractive')
jest.mock('./Runner')
jest.mock('./options')
jest.mock('./file-output')

const winston = require('winston')
const { App } = require('./App')
const { Interactive } = require('./Interactive')
const { NonInteractive, printExitSummary } = require('./NonInteractive')
const { Runner } = require('./Runner')
const { parseArgv } = require('./options')
const { openFileOutputs } = require('./file-output')
const { createPromise } = require('./util')

const mockLogger = app => {
  app.logger = {
    log: jest.fn(),
    silly: jest.fn(),
    debug: jest.fn(),
    verbose: jest.fn(),
    http: jest.fn(),
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn()
  }
}

describe('App', () => {
  beforeEach(() => {
    jest.spyOn(Date, 'now')
  })

  afterEach(() => {
    Date.now.mockRestore()
  })

  describe('constructor', () => {
    it('should record the start date', () => {
      const started = 1
      Date.now.mockReturnValueOnce(started)
      const app = new App()
      expect(app.started).toBe(started)
    })

    describe.each([
      'options',
      'logger',
      'fileDescriptors',
      'allPrefixStreams',
      'allPipesFinishedPromise',
      'runners',
      'display',
      'killed',
      'allRunnersInfo'
    ])('%s', name => {
      it('should be initialized', () => {
        const app = new App()
        expect(app[name]).not.toBe(undefined)
      })
    })
  })

  describe('shouldKeepGoing', () => {
    let runners

    beforeEach(() => {
      runners = []
      for (let x = 0; x < 10; x++) {
        const runner = {
          shouldKeepGoing: jest.fn()
        }
        runner.shouldKeepGoing.mockReturnValue(false)
        runners.push(runner)
      }
    })

    it('should return true if any individual runner should keep going', () => {
      runners[1].shouldKeepGoing.mockReturnValue(true)

      const app = new App()
      mockLogger(app)
      app.runners = runners

      expect(app.shouldKeepGoing()).toBe(true)
    })

    it('should return false if no individual runner should keep going', () => {
      const app = new App()
      mockLogger(app)
      app.runners = runners

      expect(app.shouldKeepGoing()).toBe(false)
    })
  })

  describe('exit', () => {
    let hadProcessExitCode
    let originalProcessExitCode
    let runners
    let runnerCurrentPromise
    let allPrefixStreams
    let display
    let options
    let app

    beforeEach(() => {
      hadProcessExitCode = Object.prototype.hasOwnProperty.call(process, 'exitCode')
      originalProcessExitCode = process.exitCode

      runnerCurrentPromise = createPromise()

      runners = [
        {
          current: null,
          stop: jest.fn(),
          logProperties: jest.fn()
        },
        {
          current: {
            promise: runnerCurrentPromise.promise,
            child: jest.fn(),
            kill: jest.fn(),
            started: Date.now()
          },
          stop: jest.fn(),
          logProperties: jest.fn()
        }
      ]

      allPrefixStreams = [
        {
          end: jest.fn()
        }
      ]

      display = {
        shutdown: jest.fn()
      }

      options = {
        quiet: false
      }

      app = new App()
      mockLogger(app)
      app.allRunnersInfo = false
      app.shouldKeepGoing = jest.fn()
      app.shouldKeepGoing.mockReturnValue(false)
      app.runners = runners
      app.allPrefixStreams = allPrefixStreams
      app.display = display
      app.options = options

      printExitSummary.mockClear()
    })

    afterEach(() => {
      process.exitCode = originalProcessExitCode
      if (!hadProcessExitCode) {
        delete process.exitCode
      }
    })

    it('should not perform any actions if already killed', () => {
      app.killed = true
      app.exit()
      expect(runners[0].stop).not.toHaveBeenCalled()
      expect(runners[1].stop).not.toHaveBeenCalled()
      expect(allPrefixStreams[0].end).not.toHaveBeenCalled()
      expect(display.shutdown).not.toHaveBeenCalled()
      expect(printExitSummary).not.toHaveBeenCalled()
    })

    it('should record that the app has been killed to prevent repeated kills', () => {
      expect(app.killed).toBe(false)
      app.exit()
      expect(app.killed).toBe(true)
    })

    it('should set the process exit code to 1 if we should have kept going', () => {
      app.shouldKeepGoing.mockReturnValue(true)
      app.exit()
      expect(process.exitCode).toBe(1)
    })

    it('should not set the process exit code to 1 if all runners are info', () => {
      app.shouldKeepGoing.mockReturnValue(true)
      app.allRunnersInfo = true
      app.exit()
      expect(process.exitCode).not.toBe(1)
    })

    it('should shut down all prefix streams once current runners complete', async () => {
      expect(allPrefixStreams[0].end).not.toHaveBeenCalled()
      setTimeout(runnerCurrentPromise.resolve, 1)
      await app.exit()
      expect(allPrefixStreams[0].end).toHaveBeenCalledTimes(1)
    })

    it('should stop all runners', () => {
      app.exit()
      expect(runners[0].stop).toHaveBeenCalledTimes(1)
      expect(runners[1].stop).toHaveBeenCalledTimes(1)
    })

    it('should shutdown the display', () => {
      app.exit()
      expect(display.shutdown).toHaveBeenCalledTimes(1)
    })

    it('should print the exit summary', () => {
      app.exit()
      expect(printExitSummary).toHaveBeenCalledTimes(1)
      expect(printExitSummary).toHaveBeenCalledWith(app)
    })

    it('should not print the exit summary if the quiet option is set', () => {
      options.quiet = true
      app.exit()
      expect(printExitSummary).not.toHaveBeenCalled()
    })
  })

  describe('onRunnerUpdated', () => {
    let app

    beforeEach(() => {
      app = new App()
      mockLogger(app)
      app.allRunnersInfo = false
      app.shouldKeepGoing = jest.fn()
      app.shouldKeepGoing.mockReturnValue(false)
      app.exit = jest.fn()
    })

    it('should not kill the app if already killed', () => {
      app.killed = true
      app.onRunnerUpdated()
      expect(app.exit).not.toHaveBeenCalled()
    })

    it('should not kill the app if all runners are info', () => {
      app.allRunnersInfo = true
      app.onRunnerUpdated()
      expect(app.exit).not.toHaveBeenCalled()
    })

    it('should not kill the app if any runners should keep going', () => {
      app.shouldKeepGoing.mockReturnValue(true)
      app.onRunnerUpdated()
      expect(app.exit).not.toHaveBeenCalled()
    })

    it('should kill the app if all runners are satisfied', () => {
      app.onRunnerUpdated()
      expect(app.exit).toHaveBeenCalledTimes(1)
    })
  })

  describe('setupOptions', () => {
    it('should assign the result of parseArgv to options', () => {
      parseArgv.mockReturnValue('test')

      const app = new App()
      mockLogger(app)
      app.setupOptions()

      expect(app.options).toBe('test')
    })
  })

  describe('setupLogger', () => {
    it('should assign a logger to the provided file', () => {
      const mockApp = {}
      mockLogger(mockApp)
      winston.createLogger.mockReturnValue(mockApp.logger)

      winston.format.json = jest.fn()
      winston.transports.File = jest.fn()

      const app = new App()
      app.options = { log: 'test-file.jsonl' }
      app.setupLogger()

      expect(app.logger).toBe(mockApp.logger)
      expect(winston.transports.File).toHaveBeenCalledTimes(1)
      expect(winston.transports.File).toHaveBeenCalledWith({
        filename: app.options.log
      })
    })
  })

  describe('setupFileOutputs', () => {
    beforeEach(() => {
      openFileOutputs.mockClear()
    })

    it('should set fileDescriptors, allPrefixStreams, and allPipesFinishedPromise from openFileOutputs', () => {
      openFileOutputs.mockReturnValue({
        fileDescriptors: 'testFileDescriptors',
        allPrefixStreams: 'testAllPrefixStreamsArray',
        allPipesFinishedPromise: 'testAllPipesFinishedPromise'
      })

      const app = new App()
      mockLogger(app)
      app.options = {
        commandSpecs: 'testCommandSpecs'
      }

      app.setupFileOutputs()

      expect(openFileOutputs).toHaveBeenCalledTimes(1)
      expect(openFileOutputs).toHaveBeenCalledWith('testCommandSpecs')

      expect(app.fileDescriptors).toBe('testFileDescriptors')
      expect(app.allPrefixStreams).toBe('testAllPrefixStreamsArray')
      expect(app.allPipesFinishedPromise).toBe('testAllPipesFinishedPromise')
    })
  })

  describe('setupRunners', () => {
    let app

    beforeEach(() => {
      Runner.mockClear()
      Runner.prototype.on.mockClear()

      app = new App()
      mockLogger(app)
      app.options = {
        commandSpecs: [
          'testCommand1',
          'testCommand2',
          'testCommand3'
        ]
      }
    })

    it('should instantiate a Runner for each command spec', () => {
      app.setupRunners()

      expect(Runner).toHaveBeenCalledTimes(3)
      expect(Runner).toHaveBeenCalledWith('testCommand1', app.logger)
      expect(Runner).toHaveBeenCalledWith('testCommand2', app.logger)
      expect(Runner).toHaveBeenCalledWith('testCommand3', app.logger)
    })

    it('should bind the updated event to onRunnerUpdated', () => {
      app.setupRunners()

      expect(Runner.prototype.on).toHaveBeenCalledTimes(3)
      expect(Runner.prototype.on).toHaveBeenCalledWith('updated', app.onRunnerUpdated)
    })

    it('should store the instantiated runners', () => {
      app.setupRunners()

      expect(app.runners.length).toBe(3)
      expect(app.runners[0]).toBe(Runner.mock.instances[0])
      expect(app.runners[1]).toBe(Runner.mock.instances[1])
      expect(app.runners[2]).toBe(Runner.mock.instances[2])
    })
  })

  describe('setAllRunnersInfo', () => {
    it('should set allRunnersInfo false if any runner is NOT info', () => {
      const app = new App()
      mockLogger(app)
      app.runners = [
        { spec: { mode: 'info' } },
        { spec: { mode: 'info' } },
        { spec: { mode: 'pass' } },
        { spec: { mode: 'info' } },
        { spec: { mode: 'info' } }
      ]
      app.setAllRunnersInfo()
      expect(app.allRunnersInfo).toBe(false)
    })

    it('should set allRunnersInfo true if all runners are set to mode===info', () => {
      const app = new App()
      mockLogger(app)
      app.runners = [
        { spec: { mode: 'info' } },
        { spec: { mode: 'info' } },
        { spec: { mode: 'info' } },
        { spec: { mode: 'info' } },
        { spec: { mode: 'info' } }
      ]
      app.setAllRunnersInfo()
      expect(app.allRunnersInfo).toBe(true)
    })
  })

  describe('setupDisplay', () => {
    beforeEach(() => {
      Interactive.mockClear()
      Interactive.prototype.setup.mockClear()
      NonInteractive.mockClear()
      NonInteractive.prototype.setup.mockClear()
    })

    it('should setup an interactive display if on a TTY', () => {
      const app = new App()
      mockLogger(app)
      app.options = {
        quiet: false,
        tty: true
      }

      app.setupDisplay()

      expect(Interactive).toHaveBeenCalledTimes(1)
      expect(Interactive.prototype.setup).toHaveBeenCalledTimes(1)
      expect(NonInteractive).not.toHaveBeenCalled()
      expect(NonInteractive.prototype.setup).not.toHaveBeenCalled()
    })

    it('should setup a non-interactive display if on a non-TTY', () => {
      const app = new App()
      mockLogger(app)
      app.options = {
        quiet: false,
        tty: false
      }

      app.setupDisplay()

      expect(Interactive).not.toHaveBeenCalled()
      expect(Interactive.prototype.setup).not.toHaveBeenCalled()
      expect(NonInteractive).toHaveBeenCalledTimes(1)
      expect(NonInteractive.prototype.setup).toHaveBeenCalledTimes(1)
    })

    it('should not setup a display if quiet mode is activated on a TTY', () => {
      const app = new App()
      mockLogger(app)
      app.options = {
        quiet: true,
        tty: true
      }

      app.setupDisplay()

      expect(Interactive).not.toHaveBeenCalled()
      expect(Interactive.prototype.setup).not.toHaveBeenCalled()
      expect(NonInteractive).not.toHaveBeenCalled()
      expect(NonInteractive.prototype.setup).not.toHaveBeenCalled()
    })

    it('should not setup a display if quiet mode is activated on a non-TTY', () => {
      const app = new App()
      mockLogger(app)
      app.options = {
        quiet: true,
        tty: false
      }

      app.setupDisplay()

      expect(Interactive).not.toHaveBeenCalled()
      expect(Interactive.prototype.setup).not.toHaveBeenCalled()
      expect(NonInteractive).not.toHaveBeenCalled()
      expect(NonInteractive.prototype.setup).not.toHaveBeenCalled()
    })
  })

  describe('startRunners', () => {
    it('should tell each runner to start', () => {
      const app = new App()
      mockLogger(app)
      app.runners = [
        { start: jest.fn() },
        { start: jest.fn() },
        { start: jest.fn() }
      ]

      app.startRunners()

      expect(app.runners[0].start).toHaveBeenCalledTimes(1)
      expect(app.runners[1].start).toHaveBeenCalledTimes(1)
      expect(app.runners[2].start).toHaveBeenCalledTimes(1)
    })
  })

  describe('main', () => {
    it('should call all the setup methods', () => {
      const startupMethods = [
        'setupOptions',
        'setupLogger',
        'setupFileOutputs',
        'setupRunners',
        'setAllRunnersInfo',
        'setupDisplay',
        'startRunners'
      ]

      const app = new App()
      mockLogger(app)
      startupMethods.forEach(method => {
        app[method] = jest.fn()
      })

      app.main()

      startupMethods.forEach(
        method => expect(app[method]).toHaveBeenCalledTimes(1)
      )
    })
  })
})
