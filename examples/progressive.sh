#!/usr/bin/env bash
#
# This example will queue up N commands (default: 5), which should
# progressively complete.

count=${1:-5}
commandArgs=()
for ((x=1; x<=count; x++)); do
  commandArgs=( "${commandArgs[@]}" -l "command $x" "sleep $x" )
done
set -o xtrace
npx exit-code-monitor -t $((x+1)) "${commandArgs[@]}"
true exit-code-monitor exited with code $? # xtrace will print this, but the true command will ignore the arguments
